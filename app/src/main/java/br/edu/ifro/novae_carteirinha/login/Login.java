package br.edu.ifro.novae_carteirinha.login;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import br.edu.ifro.novae_carteirinha.MainActivity;
import br.edu.ifro.novae_carteirinha.banco_de_dados_local.Aluno;
import br.edu.ifro.novae_carteirinha.download_imagem.ImageManager;
import br.edu.ifro.novae_carteirinha.tramento_dados.LeitorJSON;
import br.edu.ifro.novae_carteirinha.utilitarios.Hash;
import br.edu.ifro.novae_carteirinha.utilitarios.IntentChange;


public class Login {

    /** Método que realiza a validação dos dados de login e recebe as informações do estudante
     *
     * @param username cpf do estudante
     * @param password senha cadastrada no banco de dados do IFRO
     * @param context activity em que será executada o método
     */
    public void logar(final String username, final String password, final Context context){

        if (username.equals("") || password.equals("")){
            // Request Info
            Toast.makeText(context, "Informe úsuario/senha correto!", Toast.LENGTH_LONG).show();
        }else {
            RequestQueue queue = Volley.newRequestQueue(context);  //  cria a fila de rquisições VOLLEY

            String url = "http://koalazone.ml/api/auth"; // String do WebService IFRO

                StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>()
                    {
                        @Override
                        public void onResponse(String response) {
                            // response
                            confirmarSalvamento(context, LeitorJSON.stringToJSONObject(response));
                            Log.d("Response", response);

                        }
                    },
                    new Response.ErrorListener()
                    {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // error
                            Log.d("Error.Response", error.toString());
                            Toast.makeText(context, "ERRO AO LOGAR!\nTENTE NOVAMENTE!", Toast.LENGTH_LONG)
                                    .show();
                        }
                    }
            ) {

                @Override
                protected Map<String, String> getParams()
                {
                    Map<String, String>  params = new HashMap<String, String>();
                    params.put("user", username);
                    params.put("pass", Hash.md5Digest(password));
                    Log.d("HASH", "8fa3cf9a696c6020a38c5a4d01f8043b");
                    Log.d("PARAMS", "getParams: "+ params.toString());
                    return params;
                }

                @Override
                protected Response<String> parseNetworkResponse(NetworkResponse response) {
                    Log.e("HTTP", "parseNetworkResponse: "+ response.statusCode);
                    return super.parseNetworkResponse(response);

                }
            };
            queue.add(postRequest);
        }
    }

    /** Método que executa a classe async
     *
     * @param context contexto da activity que chama
     * @param jsonObject json contendo os dados do aluno
     */
    private void confirmarSalvamento(Context context, JSONObject jsonObject){
        AsyncSalvaDados asyncSalvaDados = new AsyncSalvaDados(context, jsonObject);
        asyncSalvaDados.execute();
    }

    private class AsyncSalvaDados extends AsyncTask<Void, Void, Boolean>{

        private  Context ctx;
        private JSONObject jsonObj;
        AsyncSalvaDados(Context context, JSONObject dados){
            ctx = context;
            jsonObj = dados;
        }

        ProgressDialog pdLoading;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pdLoading = new ProgressDialog(ctx);
            pdLoading.setMessage("\tLogando...");
            pdLoading.show();
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            Aluno alunoIFRO = new Aluno(ctx);
            ImageManager imageManager = new ImageManager();
            alunoIFRO.salvarAluno(jsonObj);//LeitorJSON.getAtributo(jsonObj, "perfil", "cpf")+
            imageManager.downloadImage("http://koalazone.ml/koala.jpg", "aluno.png", ctx);

            return alunoIFRO.existeRegistro();
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            pdLoading.dismiss();
            if (aBoolean){
                IntentChange.irPara(ctx, MainActivity.class);
            }else {
                Toast.makeText(ctx, "ERRO AO SALVAR!", Toast.LENGTH_LONG)
                        .show();
            }
            ((Activity)ctx).finish();
        }
    }

}
