package br.edu.ifro.novae_carteirinha;


import android.app.AlertDialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import br.edu.ifro.novae_carteirinha.banco_de_dados_local.Aluno;
import br.edu.ifro.novae_carteirinha.banco_de_dados_local.TableAlunos;
import br.edu.ifro.novae_carteirinha.download_imagem.ImageManager;
import br.edu.ifro.novae_carteirinha.mask.StringMask;
import br.edu.ifro.novae_carteirinha.utilitarios.ImageUtil;


/**
 * A simple {@link Fragment} subclass.
 */
public class FrenteCarteirinhaRetrato extends Fragment {

    private Aluno listaAlunos = new Aluno(getContext());
    private ImageView qrCode;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View layoutView = inflater.inflate(R.layout.fragment_frente_carteirinha_retrato, container, false);

        final ImageManager imageManager = new ImageManager();
        ImageView foto = layoutView.findViewById(R.id.foto_aluno);
        Bitmap img = imageManager.loadImage("aluno.png", getContext());
        foto.setImageBitmap(img);


        Thread thread = new Thread(new FrenteCarteirinhaRetrato.ReadDbAssyncRetrato(layoutView));

        thread.start();
        qrCode = layoutView.findViewById(R.id.qrCodeLbl);

        return layoutView;

    }

    private class ReadDbAssyncRetrato implements Runnable {

        View layout;

        public ReadDbAssyncRetrato(View layout) {
            this.layout = layout;
        }

        @Override
        public void run() {


            TextView nome = layout.findViewById(R.id.complete_name2);
            TextView dtNascimento = layout.findViewById(R.id.nascimento);
            TextView cpf = layout.findViewById(R.id.cpf);
            TextView matricula = layout.findViewById(R.id.matricula);
            final TextView instiuicao = layout.findViewById(R.id.campus);
            TextView anoDoc = layout.findViewById(R.id.ano_carteira);
            qrCode = layout.findViewById(R.id.qrCodeLbl);
            final TableAlunos aluno = listaAlunos.selecionarAluno();


            Log.e("nome", "run: "+aluno.getNomeCompleto() );
            nome.setText(aluno.getNomeCompleto());
            dtNascimento.setText(StringMask.dateMask(aluno.getDataNascimento()));
            cpf.setText(aluno.getCpf());
            matricula.setText(aluno.getMatricula());

            final String cursoS = StringMask.cursoMask(aluno.getCurso().substring(0,4)+" "+aluno.getCurso().substring(5).toLowerCase());

            final Bitmap qcode = ImageUtil.convert(aluno.getQrcode64());

            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    instiuicao.setText(String.format("%s\n\n%s\n\n%s", aluno.getCampus(), cursoS, aluno.getTurno()));
                    qrCode.setImageBitmap(qcode);

                    qrCode.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            View qrLayout = View.inflate(getContext(), R.layout.qr_alert_dialog_layout, null);
                            ImageView imageView = qrLayout.findViewById(R.id.qrcodeAlert);
                            imageView.setImageBitmap(ImageUtil.convert(aluno.getQrcode64()));
                            new AlertDialog.Builder(getContext()).setView(qrLayout).show();
                        }
                    });
                }
            });
        }
    }

}
