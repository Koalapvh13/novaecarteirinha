package br.edu.ifro.novae_carteirinha.qrcode;
import android.graphics.Bitmap;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;

public class QRCode {

    /** Método resposável por gerar o QRCode de validação
     *
     * @param mensagem informção necessária para a criação da mensagem assinada
     * @param assinatura string hexadecimal contendo a assinatura
     * @param key chave pública ECDSA
     * @return bitmap contendo um qrcode
     */
    public Bitmap gerarQRCode(String mensagem, String assinatura, String key){
        MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
        try {
            String URL_VERIFICADOR = "http://10.0.0.103/ValidadoreCarteirinha/web.jsp?msg="+mensagem+"&ass="+
                    assinatura+"&key="+key;
            BitMatrix bitMatrix = multiFormatWriter.encode(URL_VERIFICADOR,
                    BarcodeFormat.QR_CODE, 500, 500);
            BarcodeEncoder barcodeEncoder = new BarcodeEncoder();

            return barcodeEncoder.createBitmap(bitMatrix);
        }
        catch (WriterException e){
            e.printStackTrace();
        }
        return null;
    }
}
