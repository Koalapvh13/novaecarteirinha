package br.edu.ifro.novae_carteirinha;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;

import br.edu.ifro.novae_carteirinha.banco_de_dados_local.Aluno;
import br.edu.ifro.novae_carteirinha.banco_de_dados_local.TableAlunos;
import br.edu.ifro.novae_carteirinha.download_imagem.ImageManager;
import br.edu.ifro.novae_carteirinha.mask.StringMask;
import br.edu.ifro.novae_carteirinha.utilitarios.ImageUtil;
import br.edu.ifro.novae_carteirinha.utilitarios.IntentChange;

public class FrenteCarteirinha extends Fragment {

    Context context = getContext();
    Aluno listaAlunos = new Aluno(context);
    TextView  nome, dtNascimento, cpf, matricula, instiuicao, anoDoc;
    ImageView foto, qrCode;
    Bitmap qcode;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = getContext();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View layoutView = inflater.inflate(
                R.layout.fragment_frente_carteirinha, container, false);

        ImageManager imageManager = new ImageManager();
        foto = layoutView.findViewById(R.id.foto_aluno);

        Bitmap img = imageManager.loadImage("aluno.png", getContext());
        foto.setImageBitmap(img);

        Thread thread = new Thread(new ReadDbAssync(layoutView));

        thread.start();




        // Inflate the layout for this fragment
        return  layoutView;
    }

    private class ReadDbAssync implements Runnable {

        View layout;

        public ReadDbAssync(View layout) {
            this.layout = layout;
        }

        @SuppressLint("NewApi")
        @Override
        public void run() {
            nome = layout.findViewById(R.id.complete_name);
            dtNascimento = layout.findViewById(R.id.nascimento);
            cpf = layout.findViewById(R.id.cpf);
            matricula = layout.findViewById(R.id.matricula);
            instiuicao = layout.findViewById(R.id.campus);
            anoDoc = layout.findViewById(R.id.ano_carteira);
            qrCode = layout.findViewById(R.id.qrCodeLbl);

            final TableAlunos aluno = listaAlunos.selecionarAluno();

            final String cursoS = StringMask.cursoMask(aluno.getCurso().substring(0,4)+" "+aluno.getCurso().substring(5).toLowerCase());

            nome.setText(aluno.getNomeCompleto());
            dtNascimento.setText(StringMask.dateMask(aluno.getDataNascimento()));
            cpf.setText( StringMask.cpfMask(aluno.getCpf()));
            matricula.setText(aluno.getMatricula());

            qcode = ImageUtil.convert(aluno.getQrcode64());
            anoDoc.setText(String.valueOf(Calendar.getInstance().get(Calendar.YEAR)));


            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    instiuicao.setText(String.format("%s\n\n%s\n\n%s", aluno.getCampus(), cursoS, aluno.getTurno()));
                    qrCode.setImageBitmap(qcode);

                    qrCode.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            View qrLayout = View.inflate(getContext(), R.layout.qr_alert_dialog_layout, null);
                            ImageView imageView = qrLayout.findViewById(R.id.qrcodeAlert);
                            imageView.setImageBitmap(ImageUtil.convert(aluno.getQrcode64()));
                            new AlertDialog.Builder(getContext()).setView(qrLayout).setTitle("QRCode").show();

                        }
                    });
                }
            });
        }
    }
}
