package br.edu.ifro.novae_carteirinha.banco_de_dados_local;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;


/** Interface que implementa as operações de CRUD da TableAlunos
 * @author Matheus Dias Vieira (matheuspvh13@gmail.com)
 * @version 1.0
 * @since 26/02/2019
 */
@Dao
public interface DAORoom {

    @Insert
    void inserirAluno(TableAlunos tableAlunos);

    @Query("SELECT * FROM TableAlunos")
    List<TableAlunos> buscarAlunos();

    @Query("DELETE FROM TableAlunos")
    void deletarAluno();
}
