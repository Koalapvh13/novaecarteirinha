package br.edu.ifro.novae_carteirinha.download_imagem;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.artjimlop.altex.AltexImageDownloader;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import static android.content.ContentValues.TAG;

/**
 * Classe responsavel por gerenciar o acesso as imagens salvas no armazenamento interno
 * @author Matheus Dias Vieira <matheuspvh13@gmail.com>
 * @version 1.0
 */
public class ImageManager {




    /** Método responsável por salvar um bitmap como .png na memoria interna
     *
     * @param imageName nome do arquivo png a ser salvo
     * @param bitmap bitmap a ser salvo na mémoria
     */
    private void saveImage(String imageName, Bitmap bitmap, Context context){

        FileOutputStream fos;
        try {
            fos = context.openFileOutput(imageName, Context.MODE_PRIVATE);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.close();
        }
        catch (FileNotFoundException e) {
            Log.d(TAG, "file not found");
            e.printStackTrace();
        }
        catch (IOException e) {
            Log.d(TAG, "io exception");
            e.printStackTrace();
        }
    }

    /**
     * Método responsável por baixar imagem de um servidor externo e salva-la
     * na némoria interna.
     * @param url link da imagem
     * @param imageName nome a ser dado a imagem salva.
     */
    public void downloadImage(String url, final String imageName, final Context context) {
         AltexImageDownloader downloader = new AltexImageDownloader(new AltexImageDownloader.OnImageLoaderListener() {
            @Override
            public void onError(AltexImageDownloader.ImageError error) {
                Log.d("erro", "DEU Erro"+error);
            }

            @Override
            public void onProgressChange(int percent) {
                Log.d("wait", "ESPERA UM POUCO");
            }

            @Override
            public void onComplete(Bitmap result) {
                Log.d("ok", "DEU CERTO");
                saveImage(imageName, result, context);
            }
         });

         downloader.download(url, false);
    }

    /**
     * Método que busca e retorna uma imagem salva na memoria interna.
     * @param name nome da imagem a ser buscada
     * @return retorna o bitmap da imagem encontrada
     */
    public Bitmap loadImage(String name, Context context){

        FileInputStream fileInputStream;
        Bitmap bitmap = null;
        try{
            fileInputStream = context.openFileInput(name);
            bitmap = BitmapFactory.decodeStream(fileInputStream);
            fileInputStream.close();
        } catch(Exception e) {
            e.printStackTrace();
            Log.e("LOG", "loadImage: DEU ERRO FEIO");
        }
        return bitmap;
    }

    public Boolean deleteImage(String name, Context context) {
        return context.deleteFile(name);
    }
}