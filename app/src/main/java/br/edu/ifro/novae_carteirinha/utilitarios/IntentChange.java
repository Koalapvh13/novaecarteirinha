package br.edu.ifro.novae_carteirinha.utilitarios;

import android.content.Context;
import android.content.Intent;

/** Classe que abstrai o trabalho de troca de intents
 *  @author Matheus Dias Vieira (matheuspvh13@gmail.com)
 *  @version 1.0
 *  @since 03/03/2019
 */
public class IntentChange {

    /** Método responsável por realizar as transições de tela
     *
     * @param atual contexto de onde o método é chamado
     * @param destino Activity para onde o fluxo do app deve ser direcionado
     */
    public static void irPara(Context atual, Class destino){
        Intent mudanca = new Intent(atual, destino);
        atual.startActivity(mudanca);
    }
}
