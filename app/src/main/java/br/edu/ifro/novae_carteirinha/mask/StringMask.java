package br.edu.ifro.novae_carteirinha.mask;

public class StringMask {

    /** Método estático que formata a string com o nome do curso
     * @param curso string contendo o nome do curso sem formatação
     * @return string formatada do nome do curso
     */
    public static  String cursoMask(String curso){
        String[] partes = curso.split(" ");
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < partes.length; i++) {
            if (partes[i].length()>2){
                String word = partes[i];
                word = word.substring(0, 1).toUpperCase() + word.substring(1);
                sb.append(" ").append(word);
            }else {
                String word = partes[i];
                sb.append(" ").append(word);
            }

        }
        return sb.toString().replaceFirst(" ", "");
    }

    /** Método estático que formata a string com o cpf, adicionando os pontos e traço
     *
     * @param cpf string contendo o valor do cpf não formatado.
     * @return string contendo o cpf formatado (xxx.xxx.xxx-xx)
     */
    public static String cpfMask(String cpf){
        return cpf.substring(0,3)+"."+cpf.substring(3,6)+"."+cpf.substring(6,9)+"-"+cpf.substring(9,11);
    }

    /** Método estático que formata uma string para o modelo de data brasileiro (dd/MM/yyyy)
     *
     * @param data string contendo a data separada por traço (dd-MM-yyyy)
     * @return string contendo a data no formato brasileiro
     */
    public static String dateMask(String data){
        return data.substring(0,2)+"/"+data.substring(3,5)+"/"+data.substring(6);
    }
}
