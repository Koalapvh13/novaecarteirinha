package br.edu.ifro.novae_carteirinha.tramento_dados;

import org.json.JSONException;
import org.json.JSONObject;

/** Classe para leitura de JSON
 *  @author Matheus Dias Vieira (matheuspvh13@gmail.com)
 *  @version 1.0
 *  @since 03/03/2019
 */
public class LeitorJSON {

    /** Metodo estático que realiza a leitura de um atributo JSON
     *
     * @param jsonObject objeto JSON
     * @param atributo Nome do Atributo
     * @return string com o valor do atributo
     */
    public static String getAtributo(JSONObject jsonObject, String atributo){
        try {
            return jsonObject.get(atributo).toString();
        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
    }

    /** Método com a segunda implementação de getAtributo, usado para capturar valores de atributos
     * em objetos filhos
     *
     * @param objetoPai JSON Principal
     * @param nomeObjetoFilho Nome do atributo onde encontra-se o objeto filho
     * @param atributo atributo que deseja-se recuperar o valor
     * @return valor do atributo recuperado
     */
    public static String getAtributo(JSONObject objetoPai, String nomeObjetoFilho, String atributo){
        try {
            return objetoPai.getJSONObject(nomeObjetoFilho).getString(atributo);
        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
    }

    /** Método que recupera valores dentro de array de objetos
     *
     * @param jsonObject Objeto Pai
     * @param nomeArray atributo que possui o array
     * @param indice ordem do objeto no array
     * @param atributo atributo a ser recuperado
     * @return string contendo o valor do atributo buscado
     */
    public static String getArrayItem(JSONObject jsonObject, String nomeArray, int indice, String atributo){
        try {
            return jsonObject.getJSONArray(nomeArray).getJSONObject(indice).getString(atributo);
        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
    }

    /** Método estático que recebe uma string formatada e converte para um objeto JSON
     *
     * @param json string formatada
     * @return objeto JSONObject
     */
    public static JSONObject stringToJSONObject(String json){
        try {
            return new JSONObject(json);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
