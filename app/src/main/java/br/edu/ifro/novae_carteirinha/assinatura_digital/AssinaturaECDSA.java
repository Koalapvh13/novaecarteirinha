package br.edu.ifro.novae_carteirinha.assinatura_digital;

import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.EncodedKeySpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import br.edu.ifro.novae_carteirinha.utilitarios.HexConverter;

public class AssinaturaECDSA {


    private PublicKey pubK;
    private PrivateKey privK;


    public String getPubK() {
        System.out.println("publica: " + publicKeyToHexString(pubK));
        return publicKeyToHexString(pubK);
    }

    public String getPrivK() {
        return privKeyToHexString(privK);
    }

    public AssinaturaECDSA() {
        genKey();
    }

    /**
     * Método que gera as chaves publica e privada;
     *
     */
    public void genKey() {
        SecureRandom random;
        try {
            KeyPairGenerator keyGen = KeyPairGenerator.getInstance("EC");
            random = SecureRandom.getInstance("SHA1PRNG");
            keyGen.initialize(256, random);

            KeyPair pair = keyGen.generateKeyPair();
            this.privK = pair.getPrivate();
            this.pubK = pair.getPublic();
        } catch (NoSuchAlgorithmException e) {

            e.printStackTrace();
        }

    }

    /**
     * Metódo que realiza a assinatura da mensagem utilizando a chave privada e retorna uma String Base64 contendo a assinatura.
     * @param message dado que se deseja assinar
     * @param privateKey chave hexadecimal para assinar a mensagem
     * @return string BASE64 contendo a assinatura
     *
     */
    public static String sign(String privateKey, String message) {
        Signature signature;
        try {
            signature = Signature.getInstance("SHA1withECDSA");
            signature.initSign(hexStringToPrivateKey(privateKey));

            signature.update(message.getBytes());

            return HexConverter.encodeHexString(signature.sign());
        } catch (NoSuchAlgorithmException | InvalidKeyException | SignatureException e) {

            e.printStackTrace();
            return null;
        }
    }

    public static boolean validate(String publicKey, String signed, String message) {
        Signature signature;
        try {
            signature = Signature.getInstance("SHA1withECDSA");
            signature.initVerify(hexStringToPublicKey(publicKey));

            signature.update(message.getBytes());

            return signature.verify(HexConverter.decodeHexString(signed));
        } catch (NoSuchAlgorithmException | InvalidKeyException | SignatureException e) {

            e.printStackTrace();
            return false;
        }
    }


    // Métodos estáticos para conversão Keys < -- > String Hexadecimal
    private static String publicKeyToHexString(PublicKey publicKey) {
        byte[] publicKeyBytes = publicKey.getEncoded();
        return HexConverter.encodeHexString(publicKeyBytes);
    }

    private static String privKeyToHexString(PrivateKey privateKey) {
        byte[] privateKeyBytes = privateKey.getEncoded();
        return HexConverter.encodeHexString(privateKeyBytes);
    }

    private static PublicKey hexStringToPublicKey(String pubKeyHex) {

        // Convertendo os bytes salvos em inst�ncias

        byte[] publicKeyBytes = HexConverter.decodeHexString(pubKeyHex);

        KeyFactory keyFactory;
        try {
            keyFactory = KeyFactory.getInstance("EC");
            EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(publicKeyBytes);
            return keyFactory.generatePublic(publicKeySpec);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {

            e.printStackTrace();
        }
        return null;

    }

    private static PrivateKey hexStringToPrivateKey(String privKeyHex) {

        // Convertendo os bytes salvos em inst�ncias
        byte[] privateKeyBytes = HexConverter.decodeHexString(privKeyHex);

        KeyFactory keyFactory;
        try {
            keyFactory = KeyFactory.getInstance("EC");
            EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(privateKeyBytes);
            return keyFactory.generatePrivate(privateKeySpec);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {

            e.printStackTrace();
        }
        return null;
    }
}
