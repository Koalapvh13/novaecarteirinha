package br.edu.ifro.novae_carteirinha;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.concurrent.ExecutionException;

import br.edu.ifro.novae_carteirinha.banco_de_dados_local.Aluno;
import br.edu.ifro.novae_carteirinha.utilitarios.IntentChange;

public class SplashScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        boolean tela = verificarLogin();
        if (tela){
            IntentChange.irPara(SplashScreen.this, MainActivity.class);
        }else {
            IntentChange.irPara(SplashScreen.this, LoginActivity.class);
        }

        finish();
    }

    /** Método que verifica a existência de registros offline
     *
     */
    private Boolean verificarLogin(){

        LoginStatus lgStatus = new LoginStatus();
        try {
            return lgStatus.execute().get();
        } catch (ExecutionException e) {
            e.printStackTrace();
            return false;
        } catch (InterruptedException e) {
            e.printStackTrace();
            return false;
        }
    }

    private class LoginStatus extends AsyncTask<Void,Void, Boolean>{

        @Override
        protected Boolean doInBackground(Void... voids) {
            Aluno alunoIFRO = new Aluno(SplashScreen.this);
            return alunoIFRO.existeRegistro();
        }
    }
}
