package br.edu.ifro.novae_carteirinha.utilitarios;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import java.io.ByteArrayOutputStream;

/** Classe que abstrai o processo de conversão de BITMAP para BASE64
 *  @author Matheus Dias Vieira (matheuspvh13@gmail.com)
 *  @version 1.0
 *  @since 03/03/2019
 */
public class ImageUtil {

    /** Método que converte Base64 para Bitmap
     *
     * @param base64Str String contendo um Base64
     *
     * @return Bitmap
     * @throws IllegalArgumentException
     */
    public static Bitmap convert(String base64Str) throws IllegalArgumentException
    {
        byte[] decodedBytes = Base64.decode(
                base64Str.substring(base64Str.indexOf(",")  + 1),
                Base64.DEFAULT
        );

        return BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.length);
    }

    /** Método que converte Bitmap para Base64
     *
     * @param bitmap imagem em Bitmap
     * @return Base64
     */
    public static String convert(Bitmap bitmap)
    {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);

        return Base64.encodeToString(outputStream.toByteArray(), Base64.DEFAULT);
    }

}
