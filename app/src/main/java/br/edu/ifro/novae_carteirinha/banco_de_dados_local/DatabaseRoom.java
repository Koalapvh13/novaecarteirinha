package br.edu.ifro.novae_carteirinha.banco_de_dados_local;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

@Database(entities = {TableAlunos.class}, version = 2, exportSchema = false)
public abstract class DatabaseRoom extends RoomDatabase {
    public abstract DAORoom daoRoom();

    private static DatabaseRoom INSTANCE;

    public static DatabaseRoom getInMemoryDatabase(Context context){
        if (INSTANCE == null){
            INSTANCE = Room.inMemoryDatabaseBuilder(context.getApplicationContext(),
                    DatabaseRoom.class).build();
        }
        return INSTANCE;

    }

    public static DatabaseRoom getFileDatabase(Context context){
        if (INSTANCE == null){
            INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                    DatabaseRoom.class, "dados_aluno").fallbackToDestructiveMigration().build();
        }
        return INSTANCE;

    }

    public static void destroyInstance(){INSTANCE = null;}
}
