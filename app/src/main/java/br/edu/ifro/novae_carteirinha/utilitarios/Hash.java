package br.edu.ifro.novae_carteirinha.utilitarios;

import android.os.Build;
import android.support.annotation.RequiresApi;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/** Classe que implementa as funções de HASH necessária as validações do app
 * @author Matheus Dias Vieira (matheuspvh13@gmail.com)
 * @version 1.0
 * @since 01/02/2019
 */
public class Hash {

    /** Método estático para criptografar strings utilizando o algoritmo MD5
     *
     * @param mensagem texto a  ser criptografado.
     * @return String contendo o hash MD5
     */
    public static String md5Digest(final String mensagem) {
        final String MD5 = "MD5";
        try {
            // Cria o hash MD5
            MessageDigest digest = java.security.MessageDigest
                    .getInstance(MD5);
            digest.update(mensagem.getBytes());
            byte[] messageDigest = digest.digest();

            // Converte o hash em String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    /** Método estático para criptografar strings utilizando o algoritmo SHA-256
     *
     * @param mensagem texto a  ser criptografado.
     * @return String contendo o hash SHA-256
     */
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static String sha256Digest(final String mensagem){
        MessageDigest digest = null;
        try {
            digest = MessageDigest.getInstance("SHA-256");
            byte[] encodedhash = digest.digest(
                    mensagem.getBytes(StandardCharsets.UTF_8));
            return bytesToHex(encodedhash);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return "";
        }
    }

    /** Método privado utilizado para converter os bytes do hash SHA-256 em STRING.
     *
     * @param hash Hash SHA-256 em forma de array de bytes
     * @return String contendo o hash convertido.
     */
    private static String bytesToHex(byte[] hash) {
        StringBuffer hexString = new StringBuffer();
        for (int i = 0; i < hash.length; i++) {
            String hex = Integer.toHexString(0xff & hash[i]);
            if(hex.length() == 1) hexString.append('0');
            hexString.append(hex);
        }
        return hexString.toString();
    }
}
