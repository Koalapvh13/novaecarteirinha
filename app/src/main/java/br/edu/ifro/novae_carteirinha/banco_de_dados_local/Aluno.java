package br.edu.ifro.novae_carteirinha.banco_de_dados_local;

import android.annotation.SuppressLint;
import android.content.Context;

import org.json.JSONObject;

import java.util.List;

import br.edu.ifro.novae_carteirinha.assinatura_digital.AssinaturaECDSA;
import br.edu.ifro.novae_carteirinha.qrcode.QRCode;
import br.edu.ifro.novae_carteirinha.utilitarios.Hash;
import br.edu.ifro.novae_carteirinha.utilitarios.*;

import static br.edu.ifro.novae_carteirinha.tramento_dados.LeitorJSON.getArrayItem;
import static br.edu.ifro.novae_carteirinha.tramento_dados.LeitorJSON.getAtributo;

/** Classe que abstrai as operações de CRUD do banco de dados Room
 *
 */
public class Aluno {

    //Atributos
    private DatabaseRoom databaseRoom;

    // Construtor
    public Aluno(Context context){
        this.databaseRoom = DatabaseRoom.getFileDatabase(context);
    }

    //Métodos

    /** Método que salva um novo registro na tabela Aluno
     *
     * @param dadosAluno JSONObject contendo os dados sobre o  aluno
     */
    public void salvarAluno(JSONObject dadosAluno){

        AssinaturaECDSA assinatura = new AssinaturaECDSA(); // Instância um objeto responsavel por gerar as chaves
        assinatura.genKey();
        TableAlunos aluno = new TableAlunos(); // inicia o objeto de tabela

            // Atribui os valores recebidos por JSON aos respectivos campos na tabela
            aluno.setNomeCompleto(getAtributo(dadosAluno, "perfil", "nome"));
            aluno.setCpf(getAtributo(dadosAluno, "perfil", "cpf"));
            aluno.setCurso(getArrayItem(dadosAluno, "matriculas", 0, "nomeCurso"));
            aluno.setCampus(getArrayItem(dadosAluno, "matriculas", 0, "campus"));
            aluno.setDataNascimento(getAtributo(dadosAluno, "perfil", "data_nascimento"));
            aluno.setMatricula(getArrayItem(dadosAluno, "matriculas", 0, "numero"));
            aluno.setTurno(getArrayItem(dadosAluno, "matriculas", 0, "turno"));
            aluno.setPrivKey(assinatura.getPrivK());
            aluno.setPubKey(assinatura.getPubK());
            aluno.setQrcode64(genQRCode(dadosAluno, assinatura.getPrivK(), assinatura.getPubK()));

            // Salva registros na Tabela
            this.databaseRoom.daoRoom().inserirAluno(aluno);


    }

    @SuppressLint("NewApi")
    private String genQRCode(JSONObject dadosAluno, String privK, String pubK) {

        String msg = "{nome="
                +getAtributo(dadosAluno, "perfil", "nome")
                +" cpf="
                +getAtributo(dadosAluno, "perfil", "cpf")
                +" nome_curso="
                +getArrayItem(dadosAluno, "matriculas", 0, "nomeCurso")
                +" campus="
                +getArrayItem(dadosAluno, "matriculas", 0, "campus")
                +" data_nascimento="
                +getAtributo(dadosAluno, "perfil", "data_nascimento")
                +" matricula="
                +getArrayItem(dadosAluno, "matriculas", 0, "numero")
                +" turno="
                +getArrayItem(dadosAluno, "matriculas", 0, "turno")
                +"}";
        String matricula = getArrayItem(dadosAluno, "matriculas", 0, "numero");

        QRCode qrImage = new QRCode();
        return ImageUtil.convert(qrImage.gerarQRCode(matricula,  AssinaturaECDSA.sign(privK, Hash.sha256Digest(msg)), pubK));
    }

    /** Método que busca os dados do aluno cadastrado e retorna um objeto TableAluno
     *
     * @return  - TableAluno -  objeto contendo as informações do aluno
     */
    public TableAlunos selecionarAluno(){
        // Cria uma lista com os registros de aluno encontradas no banco
        List<TableAlunos> alunos =  this.databaseRoom.daoRoom().buscarAlunos();
        // Pega o item de index 0 (ZERO) da lista
        return alunos.get(0);
    }

    /** Método que deleta TODOS os registros existentes
     *
     */
    public void deletarAluno(){
        this.databaseRoom.daoRoom().deletarAluno();
    }

    public boolean existeRegistro(){
        // Cria uma lista com os registros de aluno encontradas no banco
        List<TableAlunos> alunos =  this.databaseRoom.daoRoom().buscarAlunos();

        return alunos.size() > 0;

    }
}
