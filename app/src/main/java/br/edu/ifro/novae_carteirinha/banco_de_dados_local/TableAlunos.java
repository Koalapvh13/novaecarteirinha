package br.edu.ifro.novae_carteirinha.banco_de_dados_local;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/** Classe que abstrai uma tabela usando a Library Room.
 * @author Matheus Dias Vieira (matheuspvh13@gmail.com)
 * @version 1.0
 * @since 26/02/2019
 */
@Entity
public class TableAlunos {


    // Atributos da Tabela
    @PrimaryKey
    private int id;
    private String nomeCompleto;
    private String dataNascimento;
    private String cpf;
    private String matricula;
    private String campus;
    private String curso;
    private String turno;
    private String privKey;
    private String pubKey;
    private String qrcode64;

    // Getters and Setters
    public String getNomeCompleto() {
        return nomeCompleto;
    }

    public void setNomeCompleto(String nomeCompleto) {
        this.nomeCompleto = nomeCompleto;
    }

    public String getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(String dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getPrivKey() {
        return privKey;
    }

    public void setPrivKey(String privKey) {
        this.privKey = privKey;
    }

    public String getPubKey() {
        return pubKey;
    }

    public void setPubKey(String pubKey) {
        this.pubKey = pubKey;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public String getCampus() {
        return campus;
    }

    public void setCampus(String campus) {
        this.campus = campus;
    }

    public String getCurso() {
        return curso;
    }

    public void setCurso(String curso) {
        this.curso = curso;
    }

    public String getTurno() {
        return turno;
    }

    public void setTurno(String turno) {
        this.turno = turno;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getQrcode64() {
        return qrcode64;
    }

    public void setQrcode64(String qrcode64) {
        this.qrcode64 = qrcode64;
    }
}
