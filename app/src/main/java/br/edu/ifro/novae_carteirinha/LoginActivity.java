package br.edu.ifro.novae_carteirinha;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import br.edu.ifro.novae_carteirinha.login.Login;

public class LoginActivity extends AppCompatActivity {

    // Instância dos widgets
    private EditText edTxtUser;
    private EditText edTxtPass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        edTxtUser = findViewById(R.id.edUsername);
        edTxtPass = findViewById(R.id.edPassword);
        Button btnSend = findViewById(R.id.btnSend);

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Login lg = new Login();
                lg.logar(edTxtUser.getText().toString(), edTxtPass.getText().toString(), LoginActivity.this);
            }
        });


    }
}
