package br.edu.ifro.novae_carteirinha;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.github.rongi.rotate_layout.layout.RotateLayout;

import br.edu.ifro.novae_carteirinha.banco_de_dados_local.Aluno;
import br.edu.ifro.novae_carteirinha.download_imagem.ImageManager;
import br.edu.ifro.novae_carteirinha.utilitarios.IntentChange;

public class MainActivity extends AppCompatActivity
        implements BottomNavigationView.OnNavigationItemSelectedListener {

    RotateLayout rotateLayout;
    int btnclicked = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Instância da classe Fragment e chama a classe FrenteCarteirinhaFragment(Frente da Carteirinha)
        android.support.v4.app.Fragment inicial = new FrenteCarteirinha();
        // Chama a função loadFragment() que inicia o fragment
        loadFragment(inicial);

        BottomNavigationView bottomNavigationView = findViewById(R.id.navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);

    }

    // Função que carrega os atributos necessários para a exibição do fragment
    private boolean loadFragment(android.support.v4.app.Fragment fragment) {
        //switching fragment
        if (fragment != null ) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.frame_container, fragment)
                    .commit();
            return true;
        }
        return false;
    }

    // Quando o usuario clica na tela este metodo/evento é ativado para modificar a
    // visibilidade da BottomNavigationBar
    public void onClick(View v){
        BottomNavigationView navigationBar = findViewById(R.id.navigation);
        if(navigationBar.getVisibility() == View.VISIBLE){ // Se a BottomNavigationBar está visivel
            navigationBar.setVisibility(View.INVISIBLE); // seta para invisivel
        }else{                                          // Caso Invisivel
            navigationBar.setVisibility(View.VISIBLE); // seta para visivel
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        rotateLayout = findViewById(R.id.rotateLayout);
        android.support.v4.app.Fragment fragment = null;
        switch (menuItem.getItemId())
        {
            case R.id.navigation_home:
                if (btnclicked != R.id.navigation_home) {
                    getSupportFragmentManager().popBackStack();
                    rotateLayout.setAngle(-90);
                    btnclicked = R.id.navigation_home;
                    fragment = new FrenteCarteirinha();
                }
                break;
            case R.id.navigation_notifications:
                if (btnclicked != R.id.navigation_notifications) {
                    getSupportFragmentManager().popBackStack();
                    rotateLayout.setAngle(-90);
                    btnclicked = R.id.navigation_notifications;
                    fragment = new FundoCarteirinha();
                }
                break;
            case R.id.retrato:
                if (btnclicked != R.id.retrato) {
                    AlertDialog alerta;

                    //Cria o gerador do AlertDialog
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    //define o titulo
                    builder.setTitle("Sair?");
                    //define a mensagem
                    builder.setMessage("Deseja encerrar sua sessão?");
                    //define um botão como positivo
                    builder.setPositiveButton("Cancelar", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface arg0, int arg1) {
                            arg0.dismiss();
                        }
                    });
                    //define um botão como negativo.
                    builder.setNegativeButton("Sair", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface arg0, int arg1) {
                            /*new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    Aluno a = new Aluno(MainActivity.this);
                                    ImageManager im = new ImageManager();
                                    a.deletarAluno();
                                    im.deleteImage("aluno.png", MainActivity.this);
                                    IntentChange.irPara(MainActivity.this, LoginActivity.class);
                                }
                            }).start();*/

                            new deleteAssync().execute();

                        }
                    });
                    //cria o AlertDialog
                    alerta = builder.create();
                    //Exibe
                    alerta.show();
                }
                break;
        }
        return loadFragment(fragment);
    }

    private class deleteAssync extends AsyncTask<Void, Void, Boolean>{
        ProgressDialog pdLoading = new ProgressDialog(MainActivity.this);
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pdLoading.setMessage("\tApagando...");
            pdLoading.show();
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            Aluno aluno = new Aluno(MainActivity.this);
            ImageManager imageManager = new ImageManager();
            aluno.deletarAluno();
            imageManager.deleteImage("aluno.png", MainActivity.this);
            return aluno.existeRegistro();
        }

        @Override
            protected void onPostExecute(Boolean aBoolean) {
                super.onPostExecute(aBoolean);
                pdLoading.dismiss();
                if (!aBoolean){
                    IntentChange.irPara(MainActivity.this, LoginActivity.class);
                    finish();
                }
        }
    }
}
