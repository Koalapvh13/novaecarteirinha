package br.edu.ifro.novae_carteirinha.assinatura_digital;

import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.SignatureException;

/**
 * Classe que implementa assinatura digital por meio do algoritmo DSA.
 *
 * @author Matheus Dias Vieira <matheuspvh13@gmail.com>
 * @version 1.0
 * @since 19/02/2019
 */
public class AssinaturaDSA {

    private PublicKey pubKey = null;
    private PrivateKey privKey = null;

    public PublicKey getPubKey() {
        return pubKey;
    }

    public void setPubKey(PublicKey pubKey) {
        this.pubKey = pubKey;
    }

    /**
     * Metodo que gera as chaves publica e privada;
     *
     * @throws NoSuchAlgorithmException
     */
    public void gerarChaves() throws NoSuchAlgorithmException {
        // Instancia a classe KeyPairGenerator e atribui o algoritmo DSA
        KeyPairGenerator kpg = KeyPairGenerator.getInstance("DSA");
        SecureRandom secRandom = new SecureRandom(); // Numero randomico seguro que sera usado para criar as chaves
        kpg.initialize(1024, secRandom); // Inicializa o gerador com o tamanho (1024) e o seed (secRandom) da chave
        KeyPair kpair = kpg.generateKeyPair(); // Gerada as chaves
        this.pubKey = kpair.getPublic(); // atribui o valor da chave publica ao atributo correspondente
        this.privKey = kpair.getPrivate(); // atribui o valor da chave privada ao atributo correspondente

    }

    /**
     * Metodo que realiza a assinatura da mensagem utilizando a chave privada.
     *
     * @param mensagem dado que se deseja assinar
     * @return cadeia de bytes contendo a assinatura da mensagem
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeyException
     * @throws SignatureException
     */
    public byte[] assinar(String mensagem) throws NoSuchAlgorithmException, InvalidKeyException, SignatureException {

        // Instancia a classe responsavel pela assinatura
        Signature sig = Signature.getInstance("DSA");

        // Inicializa a assinatura setando a chave privada a ser utilizada
        sig.initSign(this.privKey);

        // Assina a mensagem
        sig.update(mensagem.getBytes());
        return sig.sign();

    }

    public void validar(String mensagem, byte[] assinatura, PublicKey publicKey) throws NoSuchAlgorithmException, InvalidKeyException, SignatureException {
        Signature clienteSign = Signature.getInstance("DSA");
        clienteSign.initVerify(publicKey);
        clienteSign.update(mensagem.getBytes());

        if (clienteSign.verify(assinatura)) {
            // Confere
            System.out.print("Assinatura Valida!");
        } else {
            // NAO Confere
            System.out.print("Assinatura Invalida!");
        }

    }


}
