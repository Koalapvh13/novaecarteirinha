package br.edu.ifro.novae_carteirinha.assinatura_digital;

import android.util.Base64;

import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.EncodedKeySpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import br.edu.ifro.novae_carteirinha.utilitarios.HexConverter;

/**
 * Classe que implementa assinatura digital por meio do algoritmo DSA.
 *
 * @author Matheus Dias Vieira <matheuspvh13@gmail.com>
 * @version 1.0
 * @since 03/04/2019
 */
public class AssinaturaDigital {


    // Atributos
    private PublicKey pubKey = null;
    private PrivateKey privKey = null;


    // Getters and Setters
    public String getPubKey() {
        return AssinaturaDigital.publicKeyToBase64(pubKey);
    }

    public String getPubKeyHex() {
        return publicKeyToHexString(pubKey);
    }

    public String getPrivKey() {
        return AssinaturaDigital.privateKeyToBase64(privKey);
    }

    public String getPrivKeyHex() {
        return privKeyToHexString(privKey);
    }


    // Métodos

    /**
     * Método que gera as chaves publica e privada;
     * @throws NoSuchAlgorithmException
     */
    public void gerarChaves() throws NoSuchAlgorithmException {
        // Instância a classe KeyPairGenerator e atribui o algoritmo DSA
        KeyPairGenerator kpg = KeyPairGenerator.getInstance("DSA");
        SecureRandom secRandom = new SecureRandom(); // Número randômico seguro que será usado para criar as chaves
        kpg.initialize(1024, secRandom); // Inicializa o gerador com o tamanho (1024) e o seed (secRandom) da chave
        KeyPair kpair = kpg.generateKeyPair(); // Gerada as chaves
        this.pubKey = kpair.getPublic(); // atribui o valor da chave publica ao atributo correspondente
        this.privKey = kpair.getPrivate(); // atribui o valor da chave privada ao atributo correspondente

    }

    /**
     * Metódo que realiza a assinatura da mensagem utilizando a chave privada e retorna uma String Base64 contendo a assinatura.
     * @param mensagem dado que se deseja assinar
     * @return string BASE64 contendo a assinatura
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeyException
     * @throws SignatureException
     */
    public static String assinar(String mensagem, String privateKey) throws NoSuchAlgorithmException, InvalidKeyException, SignatureException {

        // Instância a classe responsável pela assinatura
        Signature sig = Signature.getInstance("DSA");

        // Inicializa a assinatura setando a chave privada a ser utilizada
        sig.initSign(AssinaturaDigital.base64ToPrivateKey(privateKey));

        // Assina a mensagem
        sig.update(mensagem.getBytes());
        return Base64.encodeToString(sig.sign(), Base64.URL_SAFE) ;

    }

    public static String assinarHex(String mensagem, String privateKey) throws NoSuchAlgorithmException, InvalidKeyException, SignatureException {

        // Instância a classe responsável pela assinatura
        Signature sig = Signature.getInstance("DSA");

        // Inicializa a assinatura setando a chave privada a ser utilizada
        sig.initSign(hexStringToPrivateKey(privateKey));

        // Assina a mensagem
        sig.update(mensagem.getBytes());
        return HexConverter.encodeHexString(sig.sign()) ;

    }


    /** Método que valida a assinatura digital gerada usando a Public Key
     *
     * @param mensagem
     * @param ass
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeyException
     * @throws SignatureException
     */
    public static Boolean validar(String mensagem,String ass, String publicKey) throws NoSuchAlgorithmException, InvalidKeyException, SignatureException {
        Signature clienteSign = Signature.getInstance("DSA");
        clienteSign.initVerify(AssinaturaDigital.base64ToPublicKey(publicKey));
        clienteSign.update(mensagem.getBytes());

        return clienteSign.verify(Base64.decode(ass, Base64.URL_SAFE));

    }

    public static Boolean validarHex(String mensagem,String ass, String publicKey) throws NoSuchAlgorithmException, InvalidKeyException, SignatureException {
        Signature clienteSign = Signature.getInstance("DSA");
        clienteSign.initVerify(hexStringToPublicKey(publicKey));
        clienteSign.update(mensagem.getBytes());

        return clienteSign.verify(HexConverter.decodeHexString(ass));

    }


    // Métodos estáticos privados

    /** Método Estático que converte um objeto PublicKey para uma string Base64
     *
     * @param publicKey objeto a ser convertido
     * @return string contendo a BASE64 da chave informada
     */
    private static String publicKeyToBase64(PublicKey publicKey) {
        // Convertendo os bytes salvos em instâncias
        byte[] publicKeyBytes = publicKey.getEncoded();
        return Base64.encodeToString(publicKeyBytes, Base64.URL_SAFE);
    }

    /** Método Estático que converte um objeto PrivateKey para uma string Base64
     *
     * @param privateKey objeto a ser convertido
     * @return String contendo a BASE64 da chave informada
     */
    private static String privateKeyToBase64(PrivateKey privateKey) {
        // Convertendo os bytes salvos em instâncias
        byte[] privateKeyBytes = privateKey.getEncoded();
        return Base64.encodeToString(privateKeyBytes, Base64.URL_SAFE);
    }

    /** Método Estático que retorna a string BASE64 para um OBJETO PublicKey
     *
     * @param pubKeyBase64 String BASE64
     * @return Objeto PublicKey
     */
    private static PublicKey base64ToPublicKey(String pubKeyBase64) {


        // Convertendo os bytes salvos em instâncias

        byte[] publicKeyBytes = Base64.decode(pubKeyBase64,Base64.URL_SAFE);

        KeyFactory keyFactory;
        try {
            keyFactory = KeyFactory.getInstance("DSA");
            EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(publicKeyBytes);
            return keyFactory.generatePublic(publicKeySpec);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            e.printStackTrace();
        }
        return null;


    }

    /** Método Estático que retorna a string BASE64 para um OBJETO PrivateKey
     *
     * @param privKeyBase64 String BASE64
     * @return Objeto PrivateKey
     */
    private static PrivateKey base64ToPrivateKey(String privKeyBase64) {

        // Convertendo os bytes salvos em instâncias
        byte[] privateKeyBytes = Base64.decode(privKeyBase64, Base64.URL_SAFE);


        KeyFactory keyFactory;
        try {
            keyFactory = KeyFactory.getInstance("DSA");
            EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(privateKeyBytes);
            return keyFactory.generatePrivate(privateKeySpec);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            e.printStackTrace();
        }
        return null;



    }


    private static String publicKeyToHexString(PublicKey publicKey) {
        byte[] publicKeyBytes = publicKey.getEncoded();
        return HexConverter.encodeHexString(publicKeyBytes);
    }

    private static String privKeyToHexString(PrivateKey privateKey) {
        byte[] privateKeyBytes = privateKey.getEncoded();
        return HexConverter.encodeHexString(privateKeyBytes);
    }


    private static PublicKey hexStringToPublicKey(String pubKeyHex) {


        // Convertendo os bytes salvos em instâncias

        byte[] publicKeyBytes = HexConverter.decodeHexString(pubKeyHex);

        KeyFactory keyFactory;
        try {
            keyFactory = KeyFactory.getInstance("DSA");
            EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(publicKeyBytes);
            return keyFactory.generatePublic(publicKeySpec);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            e.printStackTrace();
        }
        return null;


    }

    private static PrivateKey hexStringToPrivateKey(String privKeyHex) {

        // Convertendo os bytes salvos em instâncias
        byte[] privateKeyBytes = HexConverter.decodeHexString(privKeyHex);


        KeyFactory keyFactory;
        try {
            keyFactory = KeyFactory.getInstance("DSA");
            EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(privateKeyBytes);
            return keyFactory.generatePrivate(privateKeySpec);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            e.printStackTrace();
        }
        return null;
    }



}


